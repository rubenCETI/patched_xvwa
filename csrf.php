<?php

if (isset($_SESSION)) {
    // Comprobamos la IP con la definida durante el inicio de sesión
    if ($_SERVER['REMOTE_ADDR'] != $_SESSION['ip_address']) {
        session_unset();
        session_destroy();
    }

    // Comprobamos el user agent con el del inicio de sesión
    if ($_SERVER['HTTP_USER_AGENT'] != $_SESSION['user_agent']) {
    session_unset();
    session_destroy();
    }

    // Limitamos la duración de la sesión a una hora desde la última visita
    if (time() > ($_SESSION['last_activity'] + 3600)) {
        session_unset();
        session_destroy();
    } else {
        $_SESSION['last_activity'] = time();
    }
    
    // Definimos una rotación de IDs de sesión cada hora
    if (time() - $_SESSION['last_regenerated'] > 3600) {
        session_regenerate_id(true);
        $_SESSION['session_id'] = session_id();
        $_SESSION['last_regenerated'] = time();
    }
}