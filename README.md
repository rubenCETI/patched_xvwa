Xtreme Vulnerable Web Application (XVWA) parcheado
=========================================
XVWA es una aplicación Web mal Desarrollada en PHP / MySQL que ayuda a los entusiastas de la seguridad 
a aprender la seguridad de las aplicaciones WEB. No es recomendable alojar esta aplicación en línea, ya que está
diseñada para ser "Extremadamente Vulnerable". Recomendamos alojar esta aplicación en un entorno local/controlado.
El fin es que puedas agudizar tus habilidades de seguridad, ya que este proyecto es totalmente legal romperlo o piratearlo.
La idea es evangelizar la seguridad de las aplicaciones web para la comunidad de la forma más fácil posible.
Por favor Aprende y adquiere estas habilidades para un buen propósito. 

**Este repositorio contiene el laboratorio XVWA con las vulnerabilidades mitigadas**

<img src="https://i.imgur.com/q8wE1hY.jpg" width="900" height="700">

Se han mitigado los siguientes apartados:

1) SQL Injection – Error Based
2) SQL Injection – Blind
3) OS Command Injection
4) XPATH Injection
5) Formula Injection
6) Unrestricted File Upload
7) Reflected Cross Site Scripting
8) Stored Cross Site Scripting
9) DOM Based Cross Site Scripting
10) Server Side Request Forgery / Cross Site Port Attacks (CSRF/XSPA)
11) File Inclusion
12) Session Issues
13) Insecure Direct Object Reference
14) Missing Functional Level Access Control
15) Cross Site Request Forgery (CSRF)
16) Unvalidated Redirect & Forwards

Adicionalmente, se ha mitigado este apartado que no era obligatorio:

17) PHP Object Injection

 Buena Suerte y Happy Hacking!


## Aclaración

No aloje esta aplicación en linea ni en el entorno de producción. XVWA es una aplicación totalmente vulnerable y el acceso en línea de esta aplicación
podría llevar a un completo compromiso de su sistema. No somos responsables de dichos incidentes. Mantenerse a salvo por favor ! 

## Copyright
Este trabajo esta bajo la licencia GNU GENERAL PUBLIC LICENSE Version 3
Para ver una copia de esta licencia visita http://www.gnu.org/licenses/gpl-3.0.txt


## Instrucciones 
XVWA es fácil de instalar. Puede ser configurado en Windows, Linux o Mac. Los siguientes son los pasos básicos que debes seguir en tu entorno 
Para la instalacion. Puedes usar WAMP, XAMP o cualquier cosa  Apache-PHP-MYSQL para que funcione correctamente 

## Instalación Manual

Copie la carpeta xvwa en su directorio web. Asegúrese de que el nombre del directorio sea xvwa. 
Realice los cambios necesarios en xvwa/config.php para la conexión a la base de datos. Ejemplo a continuación:

```php
$XVWA_WEBROOT = '';  
$host = "localhost"; 
$dbname = 'xvwa';  
$user = 'root'; 
$pass = 'root';
```

Realice los siguientes cambios en el archivo de configuración de PHP.

```php
file_uploads = on 
allow_url_fopen = on 
allow_url_include = on 
```

Acceso a la Web :  http://localhost/xvwa/

Configure la base de datos y las tablas accediendo a http://localhost/xvwa/setup/

Detalles del Acceso:

```php
admin:admin
xvwa:xvwa
user:vulnerable
```

## Autores:
- @s4n7h0 https://twitter.com/s4n7h0
- @samanL33T https://twitter.com/samanl33t 
- Sebastian Veliz Donoso https://www.linkedin.com/in/sebastianvelizdonoso/

## Proyecto Original
https://github.com/s4n7h0/xvwa
