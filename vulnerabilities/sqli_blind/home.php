 

<div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">SQL Injection – Blind</a></h4>
    </div>

    </div>

    <div class="well">
        <div class="col-lg-6"> 
            <p>Search by Itemcode or use search option  
                <form method='post' action=''>
                    <div class="form-group"> 
                        <label></label>
                        <select class="form-control" name="item">
                            <option value="">Select Item Code</option>
                            <?php
                            /* Sólo activar durante depuración
                            error_reporting(E_ALL);
                            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
                            ini_set('display_errors', 1);
                            */
                            include('../../config.php');
                            $stmt = $pdo->query('SELECT itemid FROM caffaine');
                            while ($row = $stmt->fetch()) {
                                echo "<option value=\"".intval($row['itemid'])."\">".intval($row['itemid'])."</option>\n";
                            }
                            unset($stmt);

                            echo "</select><br>";
                            echo "<input class=\"form-control\" width=\"50%\" placeholder=\"Search\" name=\"search\"></input> <br>";
                            echo "<div align=\"right\"> <button class=\"btn btn-default\" type=\"submit\">Submit</button></div>";
                            echo "</div> </form> </p>";
                            echo " </div>";

                            // Recogemos los valores POST y los sanitizamos
                            if ((isset($_REQUEST['item']) && $_REQUEST['item'] != "")) { $item = intval($_REQUEST['item']); }
                            if ((isset($_REQUEST['search']) && $_REQUEST['search'] != "")) { $search = htmlspecialchars($_REQUEST['search']); }
                            $isSearch = false;


                            // Verificamos que no estén las dos variables definidas o vacías
                            if(((isset($item)) && (isset($search))) || ((!empty($item)) && (!empty($search)))){ 
                                echo "<br><ul class=\"featureList\">";
                                echo "<li class=\"cross\">Error! Can not use both search and itemcode option. Search using either of these options.</li>";
                                echo "</ul>";
                            }else if($item){
                                $stmt = $pdo->prepare('SELECT * FROM caffaine WHERE itemid = ?');
                                $stmt->execute([$item]);
                                $isSearch = true;
                            }else if($search){
                                $stmt = $pdo->prepare('SELECT * FROM caffaine WHERE itemname LIKE ? OR itemdesc LIKE ? OR categ LIKE ?');
                                $stmt->execute(["%$search%", "%$search%", "%$search%"]);
                                $isSearch = true;
                            }
                            if($isSearch){
                                echo "<table>";
                                while($rows = $stmt->fetch()){
                                    echo "<tr><td><b>Item Code : </b>".htmlspecialchars($rows['itemcode'])."</td><td rowspan=5>&nbsp;&nbsp;</td><td rowspan=5 valign=\"top\" align=\"justify\"><b>Description : </b>".htmlspecialchars($rows['itemdesc'])."</td></tr>";
                                    echo "<tr><td><b>Item Name : </b>".htmlspecialchars($rows['itemname'])."</td></tr>";
                                    echo "<td><img src='".htmlspecialchars($rows['itemdisplay'])."' height=130 weight=20/></td>";
                                    echo "<tr><td><b>Category : </b>".htmlspecialchars($rows['categ'])."</td></tr>";
                                    echo "<tr><td><b>Price : </b>".intval($rows['price'])."$</td></tr>"; 
                                    echo "<tr><td colspan=3><hr></td></tr>";
                                }
                                echo "</table>"; 
                            }

                            ?>




                            <hr>
                           
                        </div>
                    </div>
                </div>
                
                        <?php include_once('../../about.html'); ?>
