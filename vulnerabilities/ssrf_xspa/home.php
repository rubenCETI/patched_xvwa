 

 <div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">Server Side Request Forgery (SSRF/XSPA)</a></h4>
    </div>

</div>

<div class="well">
    <div class="col-lg-6"> 
        <p>Enter an image URL from remote server or internet.  
            <form method='post' action=''>
                <div class="form-group"> 
                    <label></label>
                    <input class="form-control" width="50%" placeholder="Enter URL of Image" name="img_url"></input> <br>
                    <div align="right"> <button class="btn btn-default" type="submit">Submit Button</button></div>
               </div> 
            </form>
            <?php
                $image = "";
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    if (isset($_REQUEST["img_url"])) {
                        $img = $_REQUEST["img_url"];
                        // Técnica 1: Validación de la URL
                        // Verificamos que la URL sea válida usando una expresión regular
                        if (!preg_match("/^(http|https):\/\/([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i", $img)) {
                            die("URL inválida. Por favor, comprueba que el enlace esté escrito correctamente. Únicamente se admiten URIs HTTP o HTTPS.");
                        }
                        
                        // Técnica 2: Verificación de dominios permitidos
                        // Verificamos que el dominio de la URL esté en una lista de dominios permitidos
                        /*
                        $allowed_domains = array("example.com", "example.org");
                        $host = parse_url($_POST['image_url'], PHP_URL_HOST);
                        if (!in_array($host, $allowed_domains)) {
                            die("Dominio no permitido");
                        }
                        */
                        
                        // Técnica 3: Límite de tiempo para la descarga de la imagen
                        // Establecemos un límite de tiempo para la descarga de la imagen
                        ini_set('default_socket_timeout', 2); // 2 segundos
                        
                        // Técnica 4: Verificación de cabeceras HTTP
                        // Verificamos que la respuesta HTTP tenga un código de estado 200 y que el tipo de contenido sea una imagen
                        $headers = get_headers($img, 1);
                        if (strpos($headers[0], "200 OK") === false || strpos($headers['Content-Type'], "image/") === false) {
                            die("Respuesta HTTP inválida. Por favor, comprueba que el enlace esté escrito correctamente.");
                        }
                        
                        // Técnica 5: Verificación de tamaño de archivo
                        // Verificamos que el tamaño de la imagen sea menor a un límite establecido
                        $filesize = $headers['Content-Length'];
                        if ($filesize > 2097152) { // 2 MiB
                            die("Imagen demasiado grande. Por favor, escoge una imagen menor a 2 MiB.");
                        }
                        
                        // Si todas las verificaciones son satisfactorias, mostramos la imagen y la URL
                        echo "<img src='" . $img . "' width='100' height='100' />";
                        echo "<p><strong>URL:</strong> $img</p>";
                    }
                }            
            ?>
        </p>
    </div>
      
    <hr>

</div>
<?php include_once('../../about.html'); ?>
