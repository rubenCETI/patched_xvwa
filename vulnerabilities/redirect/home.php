 <div class="thumbnail">
    <div class="caption-full">
        <h4><a href="#">Unvalidated Redirects and Forwards</a></h4>
    </div>

</div>
<div class="well">
    <p>
        <form method="get" action="">
            <div class="form-group">
                <div class="text-left">
                <h3>Major Web Application Security Consortiums</h3> <br>
                <strong>
                <a href="redirect.php?forward=https://www.owasp.org">Open Web Application Security Project</a> <br>
                <a href="redirect.php?forward=http://www.webappsec.org/">The Web Application Security Consortium (WASC)</a>
                </strong>
                <?php
                    $whitelist = [
                        'https://www.owasp.org',
                        'http://www.webappsec.org/'
                    ];
                    
                    if (isset($_GET['forward'])){
                        if (in_array($_GET['forward'], $whitelist)) { // Comprobamos que la URL esté en la whitelist
                            $forward=$_GET['forward'];
                            if (strlen($forward)>0){
                                ob_start();
                                ob_end_flush(); 
                                header("Location: ".$forward);
                            }
                        } else {
                            die("URL inválida.");
                        }
                    }
                ?>
                </div>
            </div>
        </form>
    </p>      
    <hr>
    
</div>
<?php include_once('../../about.html'); ?>