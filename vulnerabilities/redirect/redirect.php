<?php 
	$whitelist = [
		'https://www.owasp.org',
		'http://www.webappsec.org/'
	];
	
	if (isset($_GET['forward'])){
		if (in_array($_GET['forward'], $whitelist)) {
			$forward=$_GET['forward'];
			if (strlen($forward)>0){
				ob_start();
				ob_end_flush(); 
				header("Location: ".$forward);
			}
		} else {
			die("URL inválida.");
		}
	}
?>