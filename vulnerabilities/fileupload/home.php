 

<div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
      -->
      <div class="caption-full">
        <h4><a href="#">Unrestricted File Upload </a></h4>
    </div>
      </div>

      <div class="well">
        <table width="100%" style="border-collapse:collapse; table-layout:fixed;"><tr><td>
          <div class="col-lg-12"> 
            <p><h4>Add New Item to the Coffee List</h4><br>
              <form method='post' action='' enctype="multipart/form-data">
                <div class="form-group"> 
                  <label></label>
                  <span class="file-input btn btn-primary btn-file">
                   Upload Image<input type="file" name="image">
                 </span>
                 <br><br>
                 <input class="form-control" width="50%" placeholder="Item Name" name="item"></input> <br>
                 <textarea class="form-control" placeholder="Description" rows="3" name="desc"></textarea><br>
                 <input class="form-control" width="50%" placeholder="Category" name="categ"></input> <br>
                 <input class="form-control" width="50%" placeholder="Price" name="price"></input> <br>

                 <div align="right"> <button class="btn btn-default" type="submit">Submit Button</button></div>

                 <br>
               </div>
             </form>
           </p>
         </div>
       </td>
       <td>
        <div class="col-lg-12"> 
          <p><h4></h4><br>

            <?php
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
              $itemnameOk = $itemdescOk = $categOk = $priceOk = $imageOk = false;
                // Validamos los datos recibidos
                if (isset($_REQUEST["item"])) {
                  $itemname = htmlspecialchars($_REQUEST["item"]);
                  $itemnameOk = true;
                }
                if (isset($_REQUEST["desc"])) {
                  $itemdesc = htmlspecialchars($_REQUEST["desc"]);
                  $itemdescOk = true;
                }
                if (isset($_REQUEST["categ"])) {
                  $categ = htmlspecialchars($_REQUEST["categ"]);
                  $categOk = true;
                }
                if (isset($_REQUEST["price"])) {
                  $price = htmlspecialchars($_REQUEST["price"]);
                  $priceOk = true;
                }
                
                if (isset($_FILES["image"])) {
                  // Validamos tipo MIME de la imagen para que solo permita los formatos admitidos y compruebe que sea una imagen válida
                  $allowedTypes = array("image/jpeg", "image/png", "image/heic", "image/avif");
                  $regex = '/^[a-zA-Z0-9_\-\.]+$/'; // Regex para admitir sólo caracteres alfanuméricos, guiones, barras bajas y puntos.
                  
                  $fileName = basename($_FILES['image']['name']); // Obtenemos sólamente el nombre del fichero

                  if (preg_match($regex, $fileName) === 1) { // Validamos que pase el regex
                    $path = $_SERVER['DOCUMENT_ROOT'].'/xvwa/img/uploads/';
                    $path = $path . $fileName;
                    $rpath = '/xvwa/img/uploads/'. $fileName;

                    // Comprobamos MIME
                    if (isset($_FILES['image']['tmp_name'])) {
                      $fileType = mime_content_type($_FILES['image']['tmp_name']);
                      if (!in_array($fileType, $allowedTypes)) {
                          // Detiene la ejecución si no es un formato válido
                          echo "Formato no válido. Solo se permiten imágenes con formato JPEG, PNG, HEIC o AVIF (basado).";
                          die();
                      }

                      // Comprobamos que se trate de una imagen válida
                      $imageInfo = getimagesize($_FILES['image']['tmp_name']);
                      if ($imageInfo === false) {
                          // Detiene la ejecución si es una imagen malformada
                          echo "Error. Imagen corrupta.";
                          die();
                      }
                    }
                    $imageOk = true;
                  } else {
                    $imageOk = false;
                  }
                }
                
                $itemcode = "XVWA".rand(1000,9999);

                // Si todos los campos están correctos, comienza la subida del artículo
                if ($itemnameOk && $itemdescOk && $categOk && $priceOk && $imageOk) {
                  // Subimos la imagen
                  if (!move_uploaded_file($_FILES['image']['tmp_name'], $path)) {
                    echo "<h4><b><font color='red'>There was an error uploading the file, please try again!</font></b></h4>";
                  } 

                  // Insertar a BBDD
                  $stmt = $pdo->prepare("INSERT INTO caffaine (itemcode, itemname, itemdisplay, itemdesc, categ, price) VALUES (:itemcode, :itemname, :itemdisplay, :itemdesc, :categ, :price)");
                  $stmt->bindParam(':itemcode', $itemcode);
                  $stmt->bindParam(':itemname', $itemname);
                  $stmt->bindParam(':itemdisplay', $rpath);
                  $stmt->bindParam(':itemdesc', $itemdesc);
                  $stmt->bindParam(':categ', $categ);
                  $stmt->bindParam(':price', $price);
                  $stmt->execute();

                  // Y mostrar el resultado...
                  $stmt = $pdo->prepare("select itemname,itemdisplay,itemdesc,categ,price from caffaine where itemcode = :itemcode");
                  $stmt->bindParam(':itemcode',$itemcode);
                  $stmt->execute();
                  echo "<h4><b><font color='green'>Item Uploaded Successfully !!</font></b></h4><br>";
                  echo "<table>";
                  while($rows = $stmt->fetch(PDO::FETCH_NUM)){
                    echo "<tr><td><b>Code : </b>".htmlspecialchars($itemcode)."</td><td rowspan=5>&nbsp;&nbsp;</td><td rowspan=5 valign=\"top\" align=\"justify\"><b>Description : </b>".htmlspecialchars($rows[2])."</td></tr>";
                    echo "<tr><td><b>Name : </b>".htmlspecialchars($rows[0])."</td></tr>";
                    echo "<td><img src='".htmlspecialchars($rows[1])."' height=130 weight=20/></td>";
                    echo "<tr><td><b>Category : </b>".htmlspecialchars($rows[3])."</td></tr>";
                    echo "<tr><td><b>Price : </b>".htmlspecialchars($rows[4])."$</td></tr>"; 
                  }
                  echo "</table>";

                } else {
                  var_dump($itemnameOk);
                  var_dump($itemdescOk);
                  var_dump($categOk);
                  var_dump($priceOk);
                  var_dump($imageOk);
                  echo "Algunos campos son inválidos. Por favor, comprueba los datos introducidos y vuelve a intentarlo.";
                }
            }
            ?>
          </p>
        </div>
      </td></tr></table>
      <hr>
      
    </div>
    <?php include_once('../../about.html'); ?>
