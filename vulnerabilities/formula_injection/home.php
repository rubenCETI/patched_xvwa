 

<div class="thumbnail">

    <div class="caption-full">
        <h4><a href="#">CSV Formula Injection</a></h4>
    </div>

</div>

<div class="well">
    
        <p>
            <form method='post' action='export.php'>
                <div class="form-group"> 
                    <label></label>
                    <div class="form-group" align="right"> 
                        <button class="btn btn-primary" name="action" value="export" type="submit">Export to CSV</button>
                    </div>
                    <div>
                        <br>
                    <?php

                        include('../../config.php');
                          
                            if($pdo){
                                $stmt = $pdo->prepare("SELECT itemcode,itemname,categ,price from caffaine");
                                $stmt->execute();
                                echo "<table class='table table-striped'>";
                                echo "<tr><th>Item Code</th><th>Item Name</th><th>Category</th><th>Price</th></tr>";
                                while($rows=$stmt->fetch(PDO::FETCH_NUM)){
                                    echo "<tr>";
                                    echo "<td>".htmlspecialchars($rows[0])."</td>";
                                    echo "<td>".htmlspecialchars($rows[1])."</td>"; 
                                    echo "<td>".htmlspecialchars($rows[2])."</td>";
                                    echo "<td>$".htmlspecialchars($rows[3])."</td>";
                                    echo "</tr>"; 
                                }
                            }
                            echo "</table>";
                            
                            #$action = isset($_POST['action']) ? $_POST['action'] : '';
                            
                            ?>
                        </div>
                        <hr>
                </div>
            </form>
        </p>
  
</div>

<?php include_once('../../about.html'); ?>