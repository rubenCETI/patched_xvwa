<?php
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=xvwa-export.csv');

$output = fopen('php://output', 'w');

fputcsv($output, array('itemcode', 'itemname', 'categ','price'));

include('../../config.php');  
$stmt = $pdo->prepare('SELECT itemcode,itemname,categ,price from caffaine');
$stmt->execute();

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $row = array_map('htmlspecialchars', $row);
    fputcsv($output, $row);
}
?>
