 

 <div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">Cross Site Scripting (XSS) – Stored</a></h4>
    </div>


</div>

<div class="well">
    <div class="col-lg-6"> 
        <p>  <h4>Post Your Comments </h4>
            <form method='post' action=''>
                <div class="form-group"> 
                    <label></label>
                    <b>Enter Name</b>
                    <input class="form-control" placeholder="Anonymous" name="name"></input> <br>
                    <b>Enter Comment</b>
                    <textarea class="form-control" name="msg"> </textarea> <br>
                    <div align="right"> <button class="btn btn-default" type="submit">Submit Button</button></div>
               </div> 
            </form>
        </p>
    </div>
        <hr>
        <?php 
        include_once('../../config.php');

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Sanitizamos y limitamos el número de caracteres al máximo admitido de la tabla
            $user = isset($_POST['name']) ? htmlspecialchars(substr($_POST['name'],0,30)) : '';
            $comment = isset($_POST['msg']) ? htmlspecialchars(substr($_POST['msg'],0,100)) : '';
            if($comment){
                if(!$user){
                    $user = "Anonymous";
                }
                $today = date("d M Y");
                $stmt = $pdo->prepare("insert into comments (user,comment,date) values(:user,:comment,:date)");
                $stmt->bindParam(":user",$user);
                $stmt->bindParam(":comment",$comment);
                $stmt->bindParam(":date",$today);
                $stmt->execute();

            } else {
                echo "Comentario inválido";
                die();
            }
        }
       
       
        $stmt = $pdo->prepare("select user,comment,date from comments"); 
        $stmt->execute();
        while($rows = $stmt->fetch(PDO::FETCH_NUM)){
            echo "<div class=\"row\">";
                echo "<div class=\"col-md-12\">";
                echo "<span class=\"glyphicon glyphicon-star\"></span> &nbsp;";
                    echo ucfirst(htmlspecialchars($rows[0]));
                echo "<span class=\"pull-right\">".htmlspecialchars($rows[2])."</span>";
                echo "<p>".htmlspecialchars($rows[1])."</p>";
                echo "</div>";
                echo "</div><hr>";
        } 

        ?>

        <hr>

        

</div>
<?php include_once('../../about.html'); ?>