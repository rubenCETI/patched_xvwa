 

 <div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">Cross Site Scripting (XSS) – Reflected</a></h4>
    </div>

</div>

<div class="well">
    <div class="col-lg-6"> 
        <p>Enter your message here.  
            <form method='get' action=''>
                <div class="form-group"> 
                    <label></label>
                    <input class="form-control" width="50%" placeholder="Enter URL of Image" name="item"></input> <br>
                    <div align="right"> <button class="btn btn-default" type="submit">Submit Button</button></div>
               </div> 
            </form>
            <?php
                if (isset($_REQUEST['item'])) {
                    $input = $_REQUEST['item'];
                    $blacklist = array("&", "<", ">", '"', "'"); // Definimos la blacklist
                    $replacements = array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"); // Definimos los reemplazos
                    $input = str_replace($blacklist, $replacements, $input); // Reemplaza cualquier caracter prohibido por su etiqueta HTML equivalente
                    echo $input;
                }
            ?>
        </p>
    </div>
      
    <hr>
    
</div>
<?php include_once('../../about.html'); ?>
