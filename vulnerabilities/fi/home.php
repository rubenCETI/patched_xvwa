 

 <div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">File Inclusion</a></h4>
    </div>

</div>

<div class="well">

    <p>
        <form method="get" action="">
            <div class="form-group">
                <br>
                <div class="text-left">
                <?php 
                    $f='readme.txt';
                    echo "<a class=\"btn btn-primary\" href=\".?file=$f\" /> Click here </a><br><br>";

                    if (isset($_GET['file'])) {
                        $file=$_GET['file'];
                        $allowedPath = $_SERVER['DOCUMENT_ROOT'] . "/xvwa/vulnerabilities/fi/"; // Definimos la(s) ruta(s) permitida(s)

                        // Limitamos la búsqueda al directorio actual
                        $file = $_SERVER['DOCUMENT_ROOT'] . "/xvwa/vulnerabilities/fi/" . basename($file);
                        if (!file_exists($file)) {
                            die("El fichero no existe.");
                        }

                        // Comprobamos que sea un fichero regular
                        if (!is_file($file)) {
                            die("Fichero inválido.");
                        }

                        // Nos cercioramos de que se encuentra en la ruta permitida
                        $realPath = realpath($file);
                        if (strpos($realPath, $allowedPath) !== 0) {
                            die("El fichero no se encuentra en la ruta permitida.");
                        }

                        // Finalmente, nos aseguramos de que NO sea un fichero con extensión prohibida (blacklist)
                        if (!preg_match('/\.(txt|jpg|png|heic|avif|mp4|webm|mkv|xml)$/', $file)) {
                            die("El fichero tiene una extensión no permitida.");
                        }

                        include_once $realPath;
                    }                 
                ?>
                </div>
            </div>
        </form>
    </p>

      
    <hr>
    
</div>
<?php include_once('../../about.html'); ?>
