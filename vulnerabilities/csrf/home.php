<?php
session_start();
// Generamos un token CSRF
if (!isset($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
}
$csrf_token = $_SESSION['csrf_token'];
?>

<div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">Cross Site Request Forgery (CSRF)</a></h4>
    </div>

    <div class="well">
        <div class="col-lg-6"> 
            <p><h4>Change your password</h4>  
                <form method='post' action=''>
                    <div class="form-group"> 
                        <label></label>
                        <input type="password" class="form-control" width="50%" placeholder="Enter new password" name="passwd"></input> <br>
                        <input type="password" class="form-control" width="50%" placeholder="Confirm new password" name="confirm"></input> <br>
                        <input type="hidden" name="csrf_token" value="<?php echo $csrf_token; ?>">
                        <div align="right"> <button class="btn btn-default" type="submit" name="submit" value="submit">Submit Button</button></div>
                    </div> 
                </form>
                <?php
                // Comprobamos que el token CSRF coincida con el token recibido del formulario
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (!isset($_POST['csrf_token']) || $_POST['csrf_token'] !== $_SESSION['csrf_token']) {
                        die('Token CSRF inválido');
                    }
                }
                $current_user = isset($_SESSION['user']) ? $_SESSION['user'] : '' ;
                $password = isset($_POST['passwd']) ? htmlspecialchars($_POST['passwd']) : '' ;
                $confirm = isset($_POST['confirm']) ? htmlspecialchars($_POST['confirm']) : '' ;
                include('../../config.php');
                if($current_user){
                    if(isset($_POST['submit'])){
                        if(empty($password) && empty($password)){
                            echo "Passwords can not be blank !! Try Again ";
                        }else if($password != $confirm){
                            echo "Passwords don't match !! Try Again";
                        }else{
                            $stmt = $pdo->prepare("UPDATE users set password=:pass where username=:user");
                            $stmt->bindParam(':pass', md5($password));
                            $stmt->bindParam(':user', $current_user);
                            $stmt->execute(); 
                            if($stmt){
                                die("<b>Password Changed successfully<br></b>");
                            }else{
                                die("<b>Error indefinido.<br></b>");
                            }
                        }
                    }
                }else{
                    die("<b> You are not logged in.</b>");
                }
               ?>
            </p>
        </div>
        
        <hr>
        
    </div>

    <?php include_once('../../about.html'); ?>