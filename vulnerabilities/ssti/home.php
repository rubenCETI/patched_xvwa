<?php
// Killswitch guarro
http_response_code(403);
echo '<p>Lo siento, pero no puedes acceder a esta página. Sin embargo, te dejo un vídeo con el que puedes entretenerte:</p>';
echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/QqM3c_1ca1U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>';
die();
?> 

 <div class="thumbnail">
    <!--
        <img class="img-responsive" src="http://placehold.it/800x300" alt="">
    -->
    <div class="caption-full">
        <h4><a href="#">Server Side Template Injection (SSTI)</a></h4>
    </div>

</div>

<div class="well">
    <div class="col-lg-6"> 
        <p>
        Hints: <br>
        <ul>
        <li>Template Engine used is TWIG </li>
        <li>Loader function used = "Twig_Loader_String" </li>
        </ul>
        </p>
        <p>
            <form method='get' action=''>
                <div class="form-group"> 
                    <label></label>
                    <input class="form-control" width="50%" placeholder="Enter Your Name" name="name"></input> <br>
                    <div align="right"> <button class="btn btn-default" type="submit" name='submit'>Submit Button</button></div>
               </div> 
            </form>
            <?php
                if (isset($_GET['submit'])) {
                    $name=$_GET['name'];
                    // include and register Twig auto-loader
                    include 'vendor/twig/twig/lib/Twig/Autoloader.php';
                    Twig_Autoloader::register();
                    try {
                          // specify where to look for templates
                              $loader = new Twig_Loader_String();
  
                          // initialize Twig environment
                              $twig = new Twig_Environment($loader);
                         // set template variables
                         // render template
                            $result= $twig->render($name);
                            echo "Hello $result";
  
                    } catch (Exception $e) {
                          die ('ERROR: ' . $e->getMessage());
                        }
                    }

            ?>
        </p>
    </div>
      
    <hr>

</div>
<?php include_once('../../about.html'); ?>