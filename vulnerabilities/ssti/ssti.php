<?php
// Killswitch guarro
http_response_code(403);
echo '<p>Lo siento, pero no puedes acceder a esta página. Sin embargo, te dejo un vídeo con el que puedes entretenerte:</p>';
echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/QqM3c_1ca1U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>';
die();
?>
<html>
<head><title>Server Side Template injection</title></head>
<body><form action="" method="GET">
<label>Enter your Name:</label><br/><input type="text" name="name"><br><br>
<input type="submit" name="submit" value="Enter"><br><br>
</form>
<?php
if (isset($_GET['submit'])) {
$name=$_GET['name'];
// include and register Twig auto-loader
include 'vendor/twig/twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();
try {
  // specify where to look for templates
  $loader = new Twig_Loader_String();
  
  // initialize Twig environment
  $twig = new Twig_Environment($loader);
 // set template variables
 // render template
$result= $twig->render($name);
echo "Hello $result";
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}
}

?>
<p>
  <h3>Hint:</h3>
  <b>1.</b> Template Engine used is TWIG.<br>
  <b>2.</b> Loader function used = "Twig_Loader_String"<br>
</p>

</body>
</html>



