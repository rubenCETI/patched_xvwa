<?php
	session_start();

	// Definimos los diferentes campos de validación de sesión: hora de creación, última actividad, IP, user agent
	$_SESSION['last_regenerated'] = time();
	$_SESSION['last_activity'] = time();
	$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR'];
	$_SESSION['user_agent'] = $_SERVER['HTTP_USER_AGENT'];

	$uname = htmlspecialchars($_POST['username']);
	$password = md5($_POST['password']);
	$ActiveUser = '';
	include_once('config.php');
	$sql = "select username from users where username=:username and password=:password";
	$stmt = $pdo->prepare($sql);
	$stmt->bindParam(':username',$uname);
	$stmt->bindParam(':password',$password);
	$stmt->execute();
	while($rows = $stmt->fetch(PDO::FETCH_NUM)){
		$ActiveUser = $rows[0];
	}
	if(!empty($ActiveUser)){
		$_SESSION['user'] = htmlspecialchars($ActiveUser);
	}
	header("Location: /xvwa/");
	
?>
